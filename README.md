#csharp-toys
Created by Benjamin Williams  
[My GitHub Profile](https://github.com/benjaminkwilliams) | [Personal Portal](http://about.me/benjamink.williams)
## Random Toys / Utilities made with C#  
This is done for fun but if you found it helpful you can donate [here](benjaminkwilliams.blogspot.com).
* DataPivot
 * Turns a grid into a string
 * Useful for SQL "IN" WHERE clauses
* DataInsert
 * Formats a gird for SQL VALUES clause